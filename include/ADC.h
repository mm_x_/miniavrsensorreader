/*
 * ADC.h
 *
 *  Created on: 8 Jun 2011
 *      Author: u121
 */

#ifndef ADC_H_
#define ADC_H_

enum ADC_REF
{
	A_REF = 0, A_VCC = 1, A_RESERVED = 2, A_INTERNAL = 3,
};

enum ADC_PRESCALER
{
	A_P_1 = 0, A_P_2 = 1, A_P_4 = 2, A_P_8 = 3, A_P_16 = 4, A_P_32 = 5, A_P_64 = 6, A_P_128 = 7,
};

enum ADC_TRIGGER
{
	A_Free = 0,
	A_AnalogCmp = 1,
	A_ExtInt = 2,
	A_TC0CmpMatchA = 3,
	A_TC0Overflow = 4,
	A_TC1CmpMatchB = 5,
	A_TC1Overflow = 6,
	A_TC1CaptureEvent = 7
};

void adc_init(ADC_PRESCALER pr = A_P_128);
void adc_enable(int val = 1);
void adc_start(int trigger = 0);
void adc_trigger(ADC_TRIGGER trig = A_Free);

void adc_setreference(ADC_REF ref);

int adc_setchannel(int num);
int adc_getchannel(void);

void adc_enable_LeftAdjustResult(int val = 1);

/*
 * ISR(ADC_vect)
 */
void adc_enable_interrupt(int val = 1);

#endif /* ADC_H_ */
