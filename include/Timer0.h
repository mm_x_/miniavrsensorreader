/*
 * Timer0.h
 *
 *  Created on: 3 Jan 2012
 *      Author: u121
 */

#ifndef TIMER0_H_
#define TIMER0_H_

#include <stdint.h>

enum Timer0_prescaling
{
	T0_NOCLOCK = 0,
	T0_DIV1 = 1,
	T0_DIV8 = 2,
	T0_DIV64 = 3,
	T0_DIV256 = 4,
	T0_DIV1024 = 5,
	T0_EXT_FALLING = 6,
	T0_EXT_RISING = 6,
};

void timer0_init();
void timer0_clear();

void timer0_start(const int start = 1);
void timer0_stop(const int stop = 1);

void timer0_prescaling(const enum Timer0_prescaling prescaling);

/* Timer/Counter0 Overflow Interrupt Enable */
void timer0_Overflow_Int_Enable(const int enable = 1);

uint16_t timer0_get();

#endif /* TIMER0_H_ */
