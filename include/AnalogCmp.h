/*
 * AnalogCmp.h
 *
 *  Created on: 1 Jun 2011
 *      Author: u121
 */

#ifndef ANALOGCMP_H_
#define ANALOGCMP_H_

#include <avr/io.h>
#include <stdint.h>

enum COMPARATOR_INPUT
{
	ADC0 = 0, ADC1 = 1, ADC2 = 2, ADC3 = 3, ADC4 = 4, ADC5 = 5, ADC6 = 6, ADC7 = 7, AINT1 = 0xff,
};

enum COMPARATOR_INTERRUPT_MODE
{
	COMPARATOR_TOGGLE = 0, COMPARATOR_FALLING = 2, COMPARATOR_RISING = 3,
};

void comparator_init(const uint8_t input = 0);
void comparator_enable(const uint8_t val = 1);
void comparator_disable(const uint8_t val = 1);

/*
 Bit 6 – ACBG: Analog Comparator Bandgap Select
 When this bit is set, a fixed bandgap reference voltage replaces the positive input to the Analog
 Comparator. When this bit is cleared, AIN0 is applied to the positive input of the Analog Comparator.
 When the bandgap reference is used as input to the Analog Comparator, it will take a
 certain time for the voltage to stabilize. If not stabilized, the first conversion may give a wrong
 value. See ”Internal Voltage Reference” on page 51
 */
void comparator_bandgap(const uint8_t enable = 1);

/* TODO: реализовать */
void comparator_input(const uint8_t port = ADC0);
/*
 * ISR(ANA_COMP_vect)
 */
void
comparator_interrupt(const uint8_t enable = 1, const uint8_t mode = COMPARATOR_TOGGLE);

/*
 Bit 2 – ACIC: Analog Comparator Input Capture Enable
 When written logic one, this bit enables the input capture function in Timer/Counter1 to be triggered
 by the Analog Comparator. The comparator output is in this case directly connected to the
 input capture front-end logic, making the comparator utilize the noise canceler and edge select
 features of the Timer/Counter1 Input Capture interrupt. When written logic zero, no connection
 between the Analog Comparator and the input capture function exists. To make the comparator
 trigger the Timer/Counter1 Input Capture interrupt, the ICIE1 bit in the Timer Interrupt Mask
 Register (TIMSK1) must be set.

 ISR(SIG_INPUT_CAPTURE1)
 */
void comparator_capture(const uint8_t enable = 1);

#endif /* ANALOGCMP_H_ */
