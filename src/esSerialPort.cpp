/*
 * SerialPort.c
 *
 *  Created on: 01.03.2011
 *      Author: u121
 */

#include <esSerialPort.h>
#include <stdlib.h>
#include <string.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include <TE-MINI.h>

#define buff_size_max 15

#define htons(a)            ((((a)>>8)&0xff)|(((a)<<8)&0xff00))
#define ntohs(a)            htons(a)

#define htonl(a)            ( (((a)>>24)&0xff) | (((a)>>8)&0xff00) |\
                                (((a)<<8)&0xff0000) | (((a)<<24)&0xff000000) )
#define ntohl(a)            htonl(a)

struct esSerialControl
{
	void(*sendCommand)(struct esSerialControl*, int cmd, int type_arg, void * client);
	void * cliendSendCommand;
	void(*sendFreq)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client);
	void * cliendSendFreq;
	void(*sendADC)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client);
	void * cliendSendADC;

	void(*putCommand)(struct esSerialControl*, int cmd, void * buff, int type_arg, void * client);
	void * cliendPutCommand;
	void(*putFreq)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg,
			void * client);
	void * cliendPutFreq;
	void(*putADC)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg,
			void * client);
	void * cliendPutADC;

	unsigned char buff_send[buff_size_max];
	int size_send;
	int is_send;

	unsigned char buff_receive[buff_size_max];
	int size_receive;

	uint8_t err;

	uint8_t receive;
	uint8_t is_receive;
};

struct esSerialControl control;

void sendCMDError(struct esSerialControl* control, int cmd, int type_arg, void * client)
{
	esSSendError(control, esE_NOTSUPPORTED);
}
void sendError(struct esSerialControl* control, int channel, int is_norm, int type_arg, void * client)
{
	esSSendError(control, esE_NOTSUPPORTED);
}
void sendCMDError(struct esSerialControl* control, int cmd, void * buff, int type_arg, void * client)
{
	esSSendError(control, esE_NOTSUPPORTED);
}
void sendError(struct esSerialControl* control, int channel, int is_norm, void * buff, int type_arg,
		void * client)
{
	esSSendError(control, esE_NOTSUPPORTED);
}
void sendError(struct esSerialControl* control, void * buff, int type_arg, void * client)
{
	esSSendError(control, esE_NOTSUPPORTED);
}

unsigned char esSCrc8(unsigned char *pcBlock, unsigned int length)
{
	size_t len = length;
	unsigned char crc = 0xFF;
	unsigned int i;

	while (len--)
	{
		crc ^= *pcBlock++;

		for (i = 0; i < 8; i++)
			crc = crc & 0x80 ? (crc << 1) ^ 0x31 : crc << 1;
	}

	return crc;
}

void esSSetErrorMsg(struct esSerialControl * control, const char * msg)
{
}

char * esSGetLastErrorMsg(struct esSerialControl * control)
{
	if (control->err == 0)
		return "OK";
	else
		return "Error";
}

struct esSerialControl * esSInit()
{
	memset((void *) &control, 0, sizeof(struct esSerialControl));

	control.sendCommand = sendCMDError;
	control.sendFreq = sendError;
	control.sendADC = sendError;

	control.putCommand = sendCMDError;
	control.putFreq = sendError;
	control.putADC = sendError;

	esSSetBaudRate(&control, uint32_t(115200));
	esSSetCharSize(&control, 8);
	esSSetFlow(&control, esSFlowNone);
	esSSetParity(&control, esSParityNone);
	esSSetStop(&control, esSStopOne);

	control.err = 1;

	UCSR0B = (1 << RXEN0) | (1 << TXEN0);

	return &control;
}

void enableRX_INT(int val)
{
	if (val)
		UCSR0B |= _BV(RXCIE0);
	else
		UCSR0B &= ~_BV(RXCIE0);
}
void enableTX_INT(int val)
{
	if (val)
		UCSR0B |= _BV(TXCIE0);
	else
		UCSR0B &= ~_BV(TXCIE0);
}

int esSOpen(struct esSerialControl * control, char * pathRS232)
{
	control->err = 0;

	UCSR0B = _BV(TXEN0) | _BV(RXEN0);
	enableRX_INT();
	enableTX_INT();

	return 0;
}

void esSClose(struct esSerialControl * control)
{
	control->err = 1;
}

void esSClear(struct esSerialControl * control)
{

}

uint32_t esSCalcUART(uint32_t baud, uint32_t mult)
{
	uint32_t UBRR = (uint32_t(F_CPU) / (baud * mult)) - 1;
	return uint32_t(F_CPU) / (mult * (UBRR + 1));
}

int esSSetBaudRateNormal(uint32_t baud)
{
	UBRR0 = (uint32_t(F_CPU) / (baud * 16)) - 1;
	/* Normal mode, mult = 16 */
	UCSR0A &= ~_BV(U2X0);
	/* Asynchronous USART */
	UCSR0C &= ~(_BV(UMSEL00) | _BV(UMSEL01));

	return 0;
}

int esSSetBaudRateDouble(uint32_t baud)
{
	UBRR0 = (uint32_t(F_CPU) / (baud * 8)) - 1;
	/* Double mode, mult = 8 */
	UCSR0A |= _BV(U2X0);
	/* Asynchronous USART */
	UCSR0C &= ~(_BV(UMSEL00) | _BV(UMSEL01));

	return 0;
}

//int esSSetBaudRateSync(int baud)
// {
// UBRR0 = (F_CPU / (baud * 2)) - 1;
/* Normal mode, mult = 16 */
//UCSR0A &= ~_BV(U2X0);
/* Synchronous USART */
//UCSR0C |= (_BV(UMSEL00)|_BV(~UMSEL01));
//
//}
int esSSetBaudRate(struct esSerialControl * control, uint32_t baud)
{
	int d16 = abs(baud - esSCalcUART(baud, 16));
	int d8 = abs(baud - esSCalcUART(baud, 8));

	if (d8 < d16)
		esSSetBaudRateDouble(baud);
	else
		esSSetBaudRateNormal(baud);
	return 0;
}

int esSSetCharSize(struct esSerialControl * control, int size)
{
	if ((size >= 5) && (size <= 8))
	{
		UCSR0B &= ~_BV(UCSZ02);
		UCSR0C |= (size - 5) << UCSZ00;
		return 0;
	}
	else if (size == 9)
	{
		UCSR0B |= _BV(UCSZ02);
		UCSR0C |= _BV(UCSZ00) | _BV(UCSZ01);
		return 0;
	}

	return -1;
}

int esSSetFlow(struct esSerialControl * control, int flow)
{
	switch (flow)
	{
	case esSFlowNone:
		break;
	case esSFlowSoftware:
		break;
	case esSFlowHardware:
		break;
	default:
		return -1;
	}

	return 0;
}

int esSSetParity(struct esSerialControl * control, int parity)
{
	switch (parity)
	{
	case esSParityNone:
		UCSR0C &= ~(_BV(UPM00) | _BV(UPM01));
		break;
	case esSParityOdd:
		UCSR0C |= _BV(UPM01) | _BV(UPM00);
		break;
	case esSParityEven:
		UCSR0C &= ~_BV(UPM00);
		UCSR0C |= _BV(UPM01);
		break;
	default:
		return -1;
	}

	return 0;
}

int esSSetStop(struct esSerialControl * control, int stop)
{
	switch (stop)
	{
	case esSStopOne:
		UCSR0C &= ~_BV(USBS0);
		break;
	case esSStopTwo:
		return -1;
		break;
	case esSStopOnePointFive:
		UCSR0C |= _BV(USBS0);
		break;
	default:
		return -1;
	}

	return 0;
}

void esSSend(int dis_int = 1)
{
	if (dis_int)
	{
		enableTX_INT(0);
		sei();
	}

	if (control.size_send > 0)
	{
		UDR0 = control.buff_send[0];
		memcpy(control.buff_send, control.buff_send + 1, control.size_send - 1);
		control.size_send--;
		control.is_send = 1;
	}
	else
		control.is_send = 0;

	if (dis_int)
	{
		cli();
		enableTX_INT(1);
	}
}

int esSSendBuff(unsigned char * buff, int send)
{
	enableTX_INT(0);
	if ((control.size_send + send) < buff_size_max)
	{
		memcpy(control.buff_send + control.size_send, buff, send);
		control.size_send += send;

		if (!control.is_send)
			esSSend(0);

		enableTX_INT(1);
		return 0;
	}
	enableTX_INT(1);
	return -1;
}

int esSSendGetData(struct esSerialControl * control, char command, int type, int withCRC8)
{
	const unsigned char bitsGet = ((withCRC8 ? esSEnableCRC : esSDisableCRC) << esSBitsPosCRC)
			| (esSGet << esSBitsPosType) | (type << esSBitsPosLength);

	const unsigned char codeGet = (command << esSCodePosNum);

	unsigned char commandGet[] =
	{ esSFrameStart, bitsGet, codeGet, esSFrameStop, esSFrameInterlive };

	if (withCRC8)
		commandGet[4] = esSCrc8(commandGet + 1, 1);

	return esSSendBuff(commandGet, 4);
}
int esSSendGetFreq(struct esSerialControl * control, int channel, char is_norm, int type, int withCRC8)
{
	const char command = (esSFreq << esSCodePosFreq) | char(channel & esSCodeMaskNum)
			| ((is_norm ? esSNorm : esSNotNorm) << esSCodePosNorm);
	return esSSendGetData(control, command, type, withCRC8);
}

int esSSendGetADC(struct esSerialControl * control, int channel, char is_norm, int type, int withCRC8)
{
	const char command = (esSADC << esSCodePosFreq) | char(channel & esSCodeMaskNum)
			| ((is_norm ? esSNorm : esSNotNorm) << esSCodePosNorm);
	return esSSendGetData(control, command, type, withCRC8);
}
int esSSendCMDGet(struct esSerialControl * control, char command, int type, int withCRC8)
{
	return esSSendGetData(control, command | esSCodeMaskCommand, type, withCRC8);
}

int esSSendPutData(struct esSerialControl * control, char command, void * data, size_t lendata, int withCRC8)
{
	unsigned char type;
	unsigned char datanw[4];

	switch (lendata)
	{
	case 0:
		type = esSZero;
		break;
	case 1:
		type = esSByte;
		*datanw = *(unsigned char*) data;
		break;
	case 2:
		type = esSWord;
		*((uint16_t*) datanw) = htons(*(uint16_t*)data);
		break;
	case 4:
		type = esSFloat;
		*((uint32_t*) datanw) = htonl(*(uint32_t*)data);
		break;
	default:
		esSSetErrorMsg(control, "Length not supported");
		return -1;

	}
	const unsigned char bitsGet = ((withCRC8 ? esSEnableCRC : esSDisableCRC) << esSBitsPosCRC)
			| (esSPut << esSBitsPosType) | (type << esSBitsPosLength);
	const unsigned char codeGet = (command << esSCodePosNum);

	const size_t size = esSMinLengthPacket + lendata;
	unsigned char commandPutTurn[esSMaxLengthPacket];
	commandPutTurn[0] = esSFrameStart;
	commandPutTurn[1] = bitsGet;
	commandPutTurn[2] = codeGet;
	memcpy(commandPutTurn + 3, datanw, lendata);

	commandPutTurn[size - 2] = esSFrameStop;
	if (withCRC8)
		commandPutTurn[size - 1] = esSCrc8(commandPutTurn + 1, size - 3);
	else
		commandPutTurn[size - 1] = esSFrameInterlive;

	return esSSendBuff(commandPutTurn, size);
}

int esSSendError(struct esSerialControl * control, short err_no, int withCRC8)
{
	return esSSendPutData(control, esSCodeError, &err_no, sizeof(short), withCRC8);
}

int esSSendFreq(struct esSerialControl * control, int channel, void * data, size_t lendata, char is_norm,
		int withCRC8)
{
	const char command = (esSFreq << esSCodePosFreq) | char(channel & esSCodeMaskNum)
			| ((is_norm ? esSNorm : esSNotNorm) << esSCodePosNorm);
	return esSSendPutData(control, command, data, lendata, withCRC8);
}
int esSSendFreqByte(struct esSerialControl * control, int channel, char is_norm, char freq, int withCRC8)
{
	return esSSendFreq(control, channel, &freq, sizeof(freq), is_norm, withCRC8);
}
int esSSendFreqWord(struct esSerialControl * control, int channel, int16_t freq, char is_norm, int withCRC8)
{
	return esSSendFreq(control, channel, &freq, sizeof(freq), is_norm, withCRC8);
}
int esSSendFreqFloat(struct esSerialControl * control, int channel, float freq, char is_norm, int withCRC8)
{
	return esSSendFreq(control, channel, &freq, sizeof(freq), is_norm, withCRC8);
}

int esSSendADC(struct esSerialControl * control, int channel, void * data, size_t lendata, char is_norm,
		int withCRC8)
{
	const char command = (esSADC << esSCodePosFreq) | char(channel & esSCodeMaskNum)
			| ((is_norm ? esSNorm : esSNotNorm) << esSCodePosNorm);
	return esSSendPutData(control, command, data, lendata, withCRC8);
}
int esSSendADCByte(struct esSerialControl * control, int channel, char vol, char is_norm, int withCRC8)
{
	return esSSendADC(control, channel, &vol, sizeof(vol), is_norm, withCRC8);
}
int esSSendADCWord(struct esSerialControl * control, int channel, int16_t vol, char is_norm, int withCRC8)
{
	return esSSendADC(control, channel, &vol, sizeof(vol), is_norm, withCRC8);
}
int esSSendADCFloat(struct esSerialControl * control, int channel, float vol, char is_norm, int withCRC8)
{
	return esSSendADC(control, channel, &vol, sizeof(vol), is_norm, withCRC8);
}

int esSSendCMDByte(struct esSerialControl * control, char command, char data, int withCRC8)
{
	return esSSendPutData(control, command | esSCodeMaskCommand, &data, sizeof(char), withCRC8);
}
int esSSendCMDWord(struct esSerialControl * control, char command, short data, int withCRC8)
{
	return esSSendPutData(control, command | esSCodeMaskCommand, &data, sizeof(short), withCRC8);
}
int esSSendCMDFloat(struct esSerialControl * control, char command, float data, int withCRC8)
{
	return esSSendPutData(control, command | esSCodeMaskCommand, &data, sizeof(float), withCRC8);
}

int esSParse(struct esSerialControl * control, unsigned char * buff, unsigned int size)
{
	if (size == 0)
		return 0;

	if (buff[0] != esSFrameStart
	)
		return 1;

	if (size < esSMinLengthPacket
	)
		return 0;

	const int put = (buff[1] & esSBitsMaskType) >> esSBitsPosType;
	const size_t type_arg = (buff[1] & esSBitsMaskLength) >> esSBitsPosLength;

	size_t lengthData = 0;
	if (put)
		switch (type_arg)
		{
		case esSZero:
			lengthData = 0;
			break;
		case esSByte:
			lengthData = 1;
			break;
		case esSWord:
			lengthData = 2;
			break;
		case esSFloat:
			lengthData = 4;
			break;
		}

	size_t length = lengthData + esSMinLengthPacket;
	if (length > size)
		return 0;

	if (buff[length - 2] != esSFrameStop
	)
		return 1;

	if ((buff[1] & esSBitsMaskCRC) >> esSBitsPosCRC)
	{
		if (esSCrc8(buff + 1, length - 3) != buff[length - 1])
			return 1;
	}
	else
	{
		if (buff[length - 1] != 0)
			return 1;
	}

	const int isCMD = (buff[2] & esSCodeMaskCommand) >> esSCodePosCommand;
	const int vol = (buff[2] & (isCMD ? esSCodeMaskCode : esSCodeMaskNum)) >> esSCodePosNum;
	unsigned char data_out[4];
	switch (lengthData)
	{
	case 1:
		*data_out = *(buff + 3);
		break;
	case 2:
		*(uint16_t*) data_out = htons(*((uint16_t*) (buff + 3)));
		break;
	case 4:
		*(uint32_t*) data_out = htonl(*((uint32_t*) (buff + 3)));
		break;
	}

	if (isCMD)
	{
		if (put)
			control->putCommand(control, vol, data_out, type_arg, control->cliendPutCommand);
		else
			control->sendCommand(control, vol, type_arg, control->cliendSendCommand);
	}
	else
	{
		const int isFreq = (buff[2] & esSCodeMaskADCFreq) >> esSCodePosFreq;
		const int isNorm = (buff[2] & esSCodeMaskNorm) >> esSCodePosNorm;

		if (put)
		{
			if (isFreq == esSFreq
			)
				control->putFreq(control, vol, isNorm, data_out, type_arg, control->cliendPutFreq);
			else
				control->putADC(control, vol, isNorm, data_out, type_arg, control->cliendPutADC);

		}
		else
		{
			if (isFreq == esSFreq
			)
				control->sendFreq(control, vol, isNorm, type_arg, control->cliendSendFreq);
			else
				control->sendADC(control, vol, isNorm, type_arg, control->cliendSendADC);

		}
	}

	return length;
}

void esSetCallBackSendCommand(struct esSerialControl * control,
		void(*send)(struct esSerialControl*, int, int, void *), void * client)
{
	control->sendCommand = send;
	control->cliendSendCommand = client;
}
void esSetCallBackSendFreq(struct esSerialControl * control,
		void(*send)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client),
		void * client)
{
	control->sendFreq = send;
	control->cliendSendFreq = client;
}
void esSetCallBackSendADC(struct esSerialControl * control,
		void(*send)(struct esSerialControl*, int channel, int is_norm, int type_arg, void * client),
		void * client)
{
	control->sendADC = send;
	control->cliendSendADC = client;
}

void esSetCallBackReceiveCommand(struct esSerialControl * control,
		void(*put)(struct esSerialControl*, int, void *, int, void *), void * client)
{
	control->putCommand = put;
	control->cliendPutCommand = client;
}
void esSetCallBackReceiveFreq(
		struct esSerialControl * control,
		void(*put)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg,
				void * client), void * client)
{
	control->putFreq = put;
	control->cliendPutFreq = client;
}
void esSetCallBackReceiveADC(
		struct esSerialControl * control,
		void(*put)(struct esSerialControl*, int channel, int is_norm, void * buff, int type_arg,
				void * client), void * client)
{
	control->putADC = put;
	control->cliendPutADC = client;
}

void esSRemoveReceive(const size_t size)
{
	memcpy(control.buff_receive, control.buff_receive + size, control.size_receive - size);
	control.size_receive -= size;
}

void esSUpdate(struct esSerialControl * control)
{
	int needDel;
	while ((needDel = esSParse(control, control->buff_receive, control->size_receive)) != 0)
	{
		if (needDel >= control->size_receive)
			control->size_receive = 0;
		else
		{
			memcpy(control->buff_receive, control->buff_receive + needDel,
					(control->size_receive - needDel) * sizeof(char unsigned));
			control->size_receive = control->size_receive - needDel;
		}
	}
}

void esSAddReceive(struct esSerialControl * control, unsigned char receive)
{
	control->buff_receive[control->size_receive] = receive;
	control->size_receive++;
	if (control->size_receive > buff_size_max
	)
		esSRemoveReceive(1);
}

ISR(USART_RX_vect)
{
	esSAddReceive(&control, UDR0);
}

ISR(USART_TX_vect)
{
	esSSend();
}

