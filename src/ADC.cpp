/*
 * ADC.cpp
 *
 *  Created on: 8 Jun 2011
 *      Author: u121
 */

#include <ADC.h>
#include <avr/io.h>

void adc_init(ADC_PRESCALER pr)
{
	ADCSRA &= ~(0x7);
	ADCSRA |= pr << ADPS0;

	adc_enable(1);
	adc_setreference(A_REF);
	adc_setchannel(0);
	adc_enable_LeftAdjustResult(0);
	adc_enable_interrupt(0);
	adc_trigger();
}

void adc_enable(int val)
{
	if (val)
		ADCSRA |= _BV(ADEN);
	else
		ADCSRA &= ~_BV(ADEN);
}

void adc_setreference(ADC_REF ref)
{
	ADMUX &= ~(_BV(REFS0) | _BV(REFS1));
	ADMUX |= ref << REFS0;
}

int adc_setchannel(int num)
{
	if (num > 0xF)
		return false;

	ADMUX &= ~0xF;
	ADMUX |= num;
	return true;
}

int adc_getchannel(void)
{
	return ADMUX & 0b00001111;
}

void adc_enable_LeftAdjustResult(int val)
{
	if (val)
		ADMUX |= _BV(ADLAR);
	else
		ADMUX &= ~_BV(ADLAR);
}

void adc_enable_interrupt(int val)
{
	if (val)
		ADCSRA |= _BV(ADIE);
	else
		ADCSRA &= ~_BV(ADIE);
}

void adc_start(int trigger)
{
	if (trigger)
		ADCSRA |= _BV(ADATE) | _BV(ADSC);
	else
		ADCSRA |= _BV(ADSC);
}

void adc_trigger(ADC_TRIGGER trig)
{
	ADCSRB &= ~(0x7 << ADTS0);
	ADCSRB |= trig << ADTS0;
}

