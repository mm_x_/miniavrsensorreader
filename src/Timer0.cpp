/*
 * Timer0.cpp
 *
 *  Created on: 3 Jan 2012
 *      Author: u121
 */

#include <Timer0.h>
#include <avr/io.h>

Timer0_prescaling Timer0_cur_prescaling = T0_DIV1;
char Timer0_cur_start = 0;

void _timer0_prescaling(const enum Timer0_prescaling prescaling)
{
	TCCR0B &= (~(CS00 | CS01 | CS02));
	TCCR0B |= prescaling;
}

void timer0_init()
{
	timer0_clear();
}

void timer0_clear()
{
	TCNT0 = 0;
}

/* realize */
void timer0_start(const int start)
{
	Timer0_cur_start = start;
	_timer0_prescaling(Timer0_cur_start ? Timer0_cur_prescaling : T0_NOCLOCK);
}

void timer0_stop(const int stop)
{
	timer0_start(!stop);
}

void timer0_prescaling(const enum Timer0_prescaling prescaling)
{
	if (Timer0_cur_start)
		_timer0_prescaling(prescaling);

	Timer0_cur_prescaling = prescaling;
}

void timer0_Overflow_Int_Enable(const int enable)
{
	if (enable)
		TIMSK0 |= _BV(TOIE0);
	else
		TIMSK0 &= ~_BV(TOIE0);
}

uint16_t timer0_get()
{
	return TCNT0;
}

