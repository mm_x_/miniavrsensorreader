/*
 * MiniGaugeReader.c
 *
 * Created: 01/06/2011 15:32:52
 *  Author: u121
 */

#include <stdio.h>
#include <string.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#include <AnalogCmp.h>
#include <ADC.h>
#include <Timer0.h>
#include <esSerialPort.h>

const int pin_led = 13;
const int interval = 5;

int test_mode = 0;
uint8_t test_hicount = 0;

/*
 * freq data
 */
const uint16_t overflow_max = 500;

uint8_t freq_enable = 1;
uint8_t freq_test = 0;

uint16_t overflow_count = 0;

uint16_t tick_freq = 4;

uint16_t capture_count = 0;
uint32_t tick_comparator_prev = 0;

uint32_t tick_comparator = 0;
uint16_t tick_comparator_num = 0;
uint16_t prev_capture = 0;

/*
 * adc data
 */
uint16_t adc_channels_status = 0x0;
uint16_t adc_channels_request = 0;
uint16_t adc_channels_complete = 0;

int adc_type_arg[16];

uint32_t adc_vol[16];
uint16_t adc_num_vol[16];
/*
 * счетчик, когда переключить на следующий канал
 */
uint8_t adc_count[16];

/*
 * параметры текущего чтения канала ADC
 */
uint8_t adc_channel_current = 0;
int8_t adc_channel_num = 0;

/*
 int adc_ch = 0;

 int adc_ch_complete = 0;
 int adc_complete_v = 0;
 int adc_type_arg = 0;
 */

void init_timer0();

void clean_comparator()
{
	tick_comparator = 0;
	tick_comparator_num = 0;
}

uint32_t get_clean_comparator(int disint = 1)
{
	if (disint)
		cli();

	if (tick_comparator_num == 0)
	{
		if (disint)
			sei();
		return 0;
	}

	tick_comparator_prev = tick_comparator / tick_comparator_num;
	clean_comparator();

	if (disint)
		sei();
	return tick_comparator_prev;
}

float get_vol_adc(const uint16_t channel)
{
	adc_enable_interrupt(0);

	const float adc_vol_complete = float(adc_vol[channel]) / adc_num_vol[channel];
	adc_vol[channel] = 0;
	adc_num_vol[channel] = 0;
	adc_channels_complete &= ~(1 << channel);

	adc_enable_interrupt(1);
	return adc_vol_complete;
}

float tick2freq(const uint32_t ticks)
{
	if (ticks == 0)
		return 0;
	return 16000000.0f / ticks;
}

/* comparator init */
void setup_comparator(const int enable = 1)
{
	comparator_init();
	comparator_interrupt(0, COMPARATOR_FALLING);
	comparator_bandgap();
	comparator_capture(enable);
}

int check_adc_start()
{
	const int enable = adc_channels_status;
	adc_start(enable);

	return enable;
}

void adc_change_current_ch()
{
	int count = 0;

	while (count < 16)
	{
		adc_channel_current++;
		if (adc_channel_current == 16)
			adc_channel_current = 0;

		const uint8_t bits_test = (adc_channels_status >> adc_channel_current) & 1;
		if (bits_test != 0)
		{
			adc_setchannel(adc_channel_current);
			adc_start(1);
			adc_channel_num = -1;
			return;
		}

		count++;
	}

	adc_start(0);
}

void setup_adc()
{
	adc_init();
	adc_setreference(A_VCC);
	adc_setchannel(adc_channel_current);
	adc_trigger(A_TC0Overflow);
	adc_enable_interrupt();
	check_adc_start();
}

/*
 * used as input for ADC trigger
 */
void init_timer0()
{
	timer0_init();
	timer0_prescaling(T0_DIV1);
	timer0_Overflow_Int_Enable(1);
	timer0_start();
}

/*
 * used for freq count
 */
void init_timer1()
{
	TCNT1 = 0x0000;
	TCCR1A = 0;
	/* Enable:
	 * 	ICNC1: Input Capture Noise Canceler
	 * 	_BV(CS00): clk
	 */;
	TCCR1B = _BV(CS00) | _BV(ICNC1);
	TCCR1C = 0x00;

	/*
	 * Enable:
	 * 	Bit 0 � TOIE1: Timer/Counter1, Overflow Interrupt Enable
	 * 	Bit 2 � OCIE1B: Timer/Counter1, Output Compare B Match Interrupt Enable
	 *
	 */
	TIMSK1 |= _BV(TOIE1) | _BV(ICIE1);
}

/*
 * used for test mode
 */
void init_timer2()
{
	TCNT2 = 0x0000;

	/* Enable:
	 * 	_BV(CS0?): clk / 1024
	 */
	TCCR2B = _BV(CS00) | _BV(CS01) | _BV(CS02);
	TIMSK2 |= _BV(TOIE2);
}

uint16_t getTimer2()
{
	uint16_t retVal = TCNT2;
	retVal |= test_hicount << 8;
	return retVal;
}

void setup()
{
	setup_comparator();
	setup_adc();
	init_timer0();
	init_timer1();
	init_timer2();
}

ISR(TIMER1_OVF_vect)
{
	if (overflow_count > overflow_max)
	{
		clean_comparator();
		overflow_count = 0;
		prev_capture = 0;
		tick_comparator_prev = 0;
		capture_count = 0;
	}
	else
		overflow_count++;
}

ISR(TIMER2_OVF_vect)
{
	test_hicount++;
}

ISR(TIMER0_OVF_vect)
{
}

ISR(TIMER1_CAPT_vect)
{
	capture_count++;
	if (capture_count >= tick_freq)
	{
		const uint16_t cur = ICR1;
		const int64_t capture =
				overflow_count > 0 ? int64_t(0xFFFF) * overflow_count - prev_capture + cur : cur - prev_capture;
		prev_capture = cur;

		overflow_count = 0;

		tick_comparator += capture;
		++tick_comparator_num;

		capture_count = 0;
	}
}

ISR(ADC_vect)
{
	if (adc_channel_num != -1)
		if (adc_num_vol[adc_channel_current] != 0xFFFF)
		{
			adc_vol[adc_channel_current] += ADC;
			adc_num_vol[adc_channel_current]++;
		}

	adc_channel_num++;
	if (adc_channel_num >= adc_count[adc_channel_current])
	{
		adc_channels_complete |= 1 << adc_channel_current;
		adc_channel_num = 0;
		adc_change_current_ch();
	}
}

void sendCMD(struct esSerialControl* control, int cmd, int type, void * data)
{
	switch (cmd)
	{
	case esCMD_VERSION:
		esSSendCMDByte(control, esCMD_VERSION, esUART_VER);
		break;
	case esCMD_FREQ_TICK:
		esSSendCMDWord(control, esCMD_FREQ_TICK, tick_freq);
		break;
	case esCMD_TEST:
		esSSendCMDByte(control, esCMD_TEST, test_mode);
		break;
	case esCMD_ENABLE_ADC_CH:
		esSSendCMDWord(control, esCMD_ENABLE_ADC_CH, adc_channels_status);
		break;
	case esCMD_DISABLE_ADC_CH:
		esSSendCMDWord(control, esCMD_ENABLE_ADC_CH, ~adc_channels_status);
		break;
	default:
		esSSendError(control, esE_NOTSUPPORTED);
		break;
	}
}

void receiveCMD(struct esSerialControl* control, int cmd, void * buff, int type, void * client)
{
	int data = 0;
	switch (type)
	{
	case esSByte:
		data = *(uint8_t*) buff;
		break;
	case esSWord:
		data = *(uint16_t *) buff;
		break;
	case esSFloat:
		data = *(float *) buff;
		break;
	default:
		break;
	}

	if ((cmd & esCMDTypeMask) == 0)
	{
		switch (cmd)
		{
		case esCMD_VERSION:
			if (data != esUART_VER)
				esSSendError(control, esE_NOTSUPPORTED);
			else
				esSSendError(control, esE_OK);
			break;
		case esCMD_FREQ_TICK:
			tick_freq = data;
			esSSendError(control, esE_OK);
			break;
		case esCMD_TEST:
			test_mode = data;
			esSSendError(control, esE_OK);
			break;
		case esCMD_ENABLE_ADC_CH:
			if (data < 0 && data > 8)
				esSSendError(control, esE_NOTSUPPORTED);
			else
			{
				adc_channels_status |= 1 << data;
			}
			esSSendError(control, esE_OK);
			break;
		case esCMD_DISABLE_ADC_CH:
			if (data < 0 && data > 8)
				esSSendError(control, esE_NOTSUPPORTED);
			else
			{
				adc_channels_status &= ~(1 << data);
			}
			esSSendError(control, esE_OK);
			break;
		case esCMD_ENABLE_FREQ_CH:
			if(data != 0)
				esSSendError(control, esE_NOTSUPPORTED);
			else
				esSSendError(control, esE_OK);				
			break;
		case esCMD_DISABLE_FREQ_CH:
			esSSendError(control, esE_NOTSUPPORTED);
			break;
		default:
			esSSendError(control, esE_NOTSUPPORTED);
			break;
		}
	}
	else
	{
		esSSendError(control, esE_NOTSUPPORTED);
	}
}

void sendFreq(struct esSerialControl* control, int channel, int is_norm, int type_arg, void * client)
{
	if (channel != 0)
	{
		esSSendError(control, esE_NOTSUPPORTED);
		return;
	}

	const float tick = get_clean_comparator();
	const float retTick = test_mode == 1 ? float(getTimer2()) : tick;
	const float vol = is_norm == esSNorm ? tick2freq(retTick) * 60.0f : tick2freq(retTick);

	switch (type_arg)
	{
	case esSByte:
		esSSendFreqByte(control, channel,  vol, is_norm, 1);
		break;
	case esSWord:
		esSSendFreqWord(control, channel, vol, is_norm, 1);
		break;
	default:
		esSSendFreqFloat(control, channel, vol, is_norm, 1);
		break;
	}
}

void sendADC(struct esSerialControl* control, int channel, int is_norm, int type_arg, void * client)
{
	if (channel < 0 || channel > 0x0F)
	{
		esSSendError(control, esE_NOTSUPPORTED);
		return;
	}

	if (is_norm)
	{
		esSSendError(control, esE_NOTSUPPORTED);
		return;
	}

	adc_channels_request |= 1 << channel;
	adc_channels_status |= 1 << channel;

	adc_type_arg[channel] = type_arg;

	check_adc_start();
}

int main(void)
{
	memset(adc_type_arg, 0, sizeof(adc_type_arg));
	memset(adc_vol, 0, sizeof(adc_vol));
	memset(adc_num_vol, 0, sizeof(adc_num_vol));
	memset(adc_count, 32, sizeof(adc_count));

	setup();

	struct esSerialControl * control = esSInit();
	esSOpen(control, 0);

	esSetCallBackSendFreq(control, sendFreq, 0);
	esSetCallBackSendADC(control, sendADC, 0);
	esSetCallBackSendCommand(control, sendCMD, 0);
	esSetCallBackReceiveCommand(control, receiveCMD, 0);

	sei();
	
	
	/*
	// set test mode
	esSAddReceive(control, 127);
	esSAddReceive(control, 193);
	esSAddReceive(control, 131);
	esSAddReceive(control, 1);
	esSAddReceive(control, 255);
	esSAddReceive(control, 21);
	esSUpdate(control);
	*/
	
	while (1)
	{
		for (uint8_t cur_ch = 0; cur_ch <= 0x0F; cur_ch++)
		{
			cli();
			if ((adc_channels_request >> cur_ch & 1) != 0 && (adc_channels_complete >> cur_ch & 1) != 0)
			{
				const float vol = get_vol_adc(cur_ch);
				const float retVal = test_mode == 1 ? float(getTimer2()) * float(1024) / float(0xFFFF) : vol;
				adc_channels_request &= ~(1 << cur_ch);
				const int type = adc_type_arg[cur_ch];
				sei();
				
				switch (type)
				{
				case esSByte:
					esSSendADCByte(control, cur_ch, retVal, 0, 1);
					break;
				case esSWord:
					esSSendADCWord(control, cur_ch, retVal, 0, 1);
					break;
				default:
					esSSendADCFloat(control, cur_ch, retVal, 0, 1);
					break;
				}
			}
			sei();
		}

		esSUpdate(control);
	}

	return 0;
}
