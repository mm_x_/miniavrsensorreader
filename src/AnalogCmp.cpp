/*
 * AnalogCmp.cpp
 *
 *  Created on: 1 Jun 2011
 *      Author: u121
 */

#include "AnalogCmp.h"

void comparator_init(const uint8_t input)
{
	comparator_enable();
}

void comparator_enable(const uint8_t val)
{
	if (!val)
		ACSR |= (1 << ACD);
	else
		ACSR &= ~(1 << ACD);
}

void comparator_disable(const uint8_t val)
{
	comparator_enable(!val);
}

void comparator_bandgap(const uint8_t enable)
{
	if (enable)
		ACSR |= (1 << ACBG);
	else
		ACSR &= ~(1 << ACBG);
}

void comparator_input(const uint8_t port)
{

}

void comparator_interrupt(const uint8_t enable, const uint8_t mode)
{
	if (enable)
		ACSR |= (1 << ACIE);
	else
		ACSR &= ~(1 << ACIE);

	ACSR += mode;
}

void comparator_capture(const uint8_t enable)
{
	if (enable)
		ACSR |= (1 << ACIC);
	else
		ACSR &= ~(1 << ACIC);
}
